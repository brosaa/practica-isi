import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;
import java.lang.IllegalArgumentException;

public class DescuentoBlackFridayTest {
	
	private double precioInicial;
	private double Descuento_total;
	private Calendar fecha;
	
	// Comprueba que introducimos una fecha y descuento correctos
	@Test
	public void TestDescuentoOk() {
		precioInicial = 230;
		Descuento_total = 30;
		Calendar fecha = new GregorianCalendar(2021,11,29);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
	
	// Comprueba que introducimos una fecha correcta y no hay descuento 
	@Test
	public void TestEsBlackFridayNoDescuento() {
		precioInicial = 230;
		Descuento_total = 0;
		Calendar fecha = new GregorianCalendar(2021,11,29);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
	
	// Comprueba que introducimos una fecha correcta y descuento negativo
	@Test(expected=IllegalArgumentException.class)
	public void TestDescuentoNegativo() {
		precioInicial = 230;
		Descuento_total = -3;
		Calendar fecha = new GregorianCalendar(2021,11,29);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
	
	// Comprueba que introducimos una fecha incorrecta y descuento correcto
	@Test
	public void TestNoBlackFriday_NoDescuento() {
		precioInicial = 230;
		Descuento_total = 0;
		Calendar fecha = new GregorianCalendar(2021,4,4);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
	
	// Comprueba que introducimos una fecha incorrecta y descuento negativo
	@Test(expected=IllegalArgumentException.class)
	public void TestNoBlackFriday_DescuentoMal() {
		precioInicial = 230;
		Descuento_total = -1;
		Calendar fecha = new GregorianCalendar(2021,4,4);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
	// Comprueba que introducimos una fecha que no existe y descuento incorrecto
	@Test(expected=IllegalArgumentException.class)
	public void TestFechaMal_DescuentoMal() {
		precioInicial = 230;
		Descuento_total = -1;
		Calendar fecha = new GregorianCalendar(2021,4,0);
		DescuentoBlackFriday.precioFinal(precioInicial, Descuento_total, fecha);
	}
}
