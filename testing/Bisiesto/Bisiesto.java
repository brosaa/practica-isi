import java.lang.IllegalArgumentException;
import java.util.GregorianCalendar;

public class Bisiesto{
	public static boolean esAñoBisiesto(int año) throws IllegalArgumentException {
		boolean bisiesto = false;
		if(año > 0) {
			if ((año % 4 == 0) && ((año % 100 != 0) || (año % 400 == 0)))
				bisiesto = true;
			else
				bisiesto = false;
		}else {
			throw new IllegalArgumentException("Año incorrecto");
		}
		return bisiesto;
	}
}
