public class Embotelladora {

	// @param	pequenas: número de botellas en almacén de 1L
	//		grandes : número de botellas en almacén de 5L
	//		total : número total de litros que hay que embotellar
	// @return 	número de botellas pequeñas necesarias para embotellar el total de lı́quido, teniendo
	// 		en cuenta que hay que minimizar el número de botellas pequeñas: primero
	// 		se rellenan las grandes
	// @throws 	NoSolution si no es posible embotellar todo el lı́quido

	public static int calcularNumBotellasPequenas(int pequenas, int grandes, int total) throws NoSolution {

		int botGrandes = 0;
		//Introducen un número de botellas (pequenas o grandes) o un número de litros no válido
		if (pequenas < 0 || grandes < 0 || total <= 0){
			throw new NoSolution("Invalid parameter");
		}
		
		if (grandes != 0 && total >= 5){ 
			botGrandes = Math.min((int)Math.floor(total / grandes), grandes); 
		}
		int resto = total - botGrandes * 5;
		int botPequeñas = Math.min(resto, pequenas); 
		resto = resto - botPequeñas;

		if (resto != 0){
			throw new NoSolution("Not all liters can be bottled");
		}
		
		return botPequeñas;
	}
}
