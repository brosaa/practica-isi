import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class EmbotelladoraTest {
	
	private int litros;  
	private int pequenas;
	private int grandes;
	
	//TESTS PARA LANZAR LA EXCEPCIÓN NoSolution
	//número de botellas de 5 y 1 igual a 0
	@Test(expected=NoSolution.class)
	public void TestNoBotellas() throws NoSolution
	{
		pequenas = 0;
		grandes = 0;
		litros= 5;
		Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros);
	}
	//no hay suficientes botellas para embotellar
	@Test(expected=NoSolution.class)
	public void ceroGrandesNoSufPequenas() throws NoSolution
	{
		pequenas = 2;
		grandes = 0;
		litros= 5;
		Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros);
	}
	
	//no hay suficientes botellas para embotellar
	@Test(expected=NoSolution.class)
	public void ceroPequenasNoSufGrandes() throws NoSolution
	{
		pequenas = 0;
		grandes = 1;
		litros= 7;
		Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros);
	}
	//número de litros igual a 0
	@Test(expected=NoSolution.class)
	public void nadaParaEmbotellar() throws NoSolution
	{
		pequenas = 0;
		grandes = 1;
		litros= 0;
		Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros);
	}
	
	//parámetros inválidos
	@Test(expected=NoSolution.class)
	public void parametrosInvalidos() throws NoSolution
	{
		pequenas = -1;
		grandes = -1;
		litros= 0;
		Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros);
	}
	
	//TESTS QUE NO LANZAN LA EXCEPCIÓN
	//Litros múltiplo de 5, no hay botellas de 5l pero sí hay suficientes de 1l
	@Test
	public void ceroGrandesSufPequenas() throws NoSolution
	{
		pequenas = 8;
		grandes = 0;
		litros= 5;
		assertEquals(5,Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros));
	}
	
	//Litros múltiplo de 5, sí hay botellas de 5l y no hay de 1l
	@Test
	public void ceroPequenasSufGrandes() throws NoSolution
	{
		pequenas = 0;
		grandes = 1;
		litros= 5;
		assertEquals(0,Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros));
	}
	
	//Litros múltiplo de 5, sí hay botellas de 5l y sí hay de 1l
	@Test
	public void multiplo5SufBotellas() throws NoSolution
	{
		pequenas = 10;
		grandes = 1;
		litros= 5;
		assertEquals(0,Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros));
	}
	
	//Litros no múltiplo de 5, sí hay suficientes botellas de 5l y sí hay suficientes de 1l
	@Test
	public void noMultiplo5SufBotellas() throws NoSolution
	{
		pequenas = 10;
		grandes = 1;
		litros= 7;
		assertEquals(2,Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros));
	}
	
	//Litros no múltiplo de 5, no botellas de 5l y sí hay suficientes de 1l
	@Test
	public void noMultiplo5CeroGrandesSufPequenas() throws NoSolution
	{
		pequenas = 10;
		grandes = 0;
		litros= 7;
		assertEquals(7,Embotelladora.calcularNumBotellasPequenas(pequenas, grandes, litros));
	}
}
